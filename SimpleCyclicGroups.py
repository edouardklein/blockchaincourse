from sympy import primerange, nextprime
import timeit
from tqdm import tqdm


fmt = '{b:>13}^{e:>13}={r:>13}[{n:>13}]'

def is_simple_cyclic_group(b, n):
    "Return True if b is a generator of (Z/nZ)^x. n is assumed to be prime."
    powers = set([1, b])
    for e in tqdm(range(2, n)):
        r = pow(b, e, n)
        #print(fmt.format(b=b, e=e, r=r, n=n))
        if r in powers:
            #print(f'\t Cycling, we stop')
            break
        if r == 0:
            #print(f'\t0, we stop, we stop')
            break
        powers.add(r)
    if len(powers) == n-1:
        #print(f'{b} is a generator of the simple cyclic group (Z/{n}Z)^x')
        with open('FullGroups.txt', 'a') as f:
            f.write(f'{n:>14}\t{b:>14}\n')
        return True
    return False

n = 10000000

while True:
    n = nextprime(n)
    for i in range(2, n):
        print(i)
        if is_simple_cyclic_group(i, n):
            break
